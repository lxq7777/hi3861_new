#ifndef __MAIN_HEADER_H__
#define __MAIN_HEADER_H__


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <hi_stdlib.h>

#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_uart.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "step_motor.h"
#include "myapp_log.h"
#include "uart.h"
#include "net_demo.h"
#include "net_params.h"
#include "wifi_connecter.h"

#endif