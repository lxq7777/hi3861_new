#ifndef __UART_H__
#define __UART_H__

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include <hi_uart.h>
#include <hi_stdlib.h>
// #include <hisignalling_protocol.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_uart.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "step_motor.h"
#include "myapp_log.h"


#define DEMO_UART_NUM HI_UART_IDX_1
#define UART_BUFF_SIZE 32768
#define WRITE_BY_INT
#define UART_DEMO_TASK_STAK_SIZE 2048
#define UART_DEMO_TASK_PRIORITY 25

typedef enum
{
    UART_RECEIVE_FLAG = 0,
    UART_RECVIVE_LEN,
    UART_SEND_FLAG = 2,
    UART_SEND_LEN
} UartDefType;

typedef enum
{
    UART_RECV_TRUE = 0,
    UART_RECV_FALSE,
} UartRecvDef;

typedef struct
{
    unsigned int uartChannel;
    unsigned char g_receiveUartBuff[UART_BUFF_SIZE];
    int g_uartReceiveFlag;
    int g_uartLen;
} UartDefConfig;

void Uart1GpioConfig(void);
int SetUartRecvFlag(UartRecvDef def,UartDefConfig uartDefConfig);
int GetUartConfig(UartDefType type,UartDefConfig uartDefConfig);
void ResetUartReceiveMsg(UartDefConfig uartDefConfig);
unsigned char *GetUartReceiveMsg(UartDefConfig uartDefConfig);
void UartInit(void);



#endif