#ifndef __STEP_MOTOR_H__
#define __STEP_MOTOR_H__

#include "iot_gpio.h"
#include "myapp_log.h"
#include <unistd.h>

void step_motor_init(void);

void step_monitor_set_dir_up(void);
void step_monitor_set_dir_left(void);
void step_monitor_set_dir_right(void);
void step_monitor_set_dir_down(void);

void step_monitor_exe_both_step_one_cycle(int sleep_time);
void step_monitor_exe_left_step_one_cycle(int sleep_time);
void step_monitor_exe_right_step_one_cycle(int sleep_time);

void step_monitor_exe_both_step_params_cylce(int cycles, int sleep_time);
void step_monitor_exe_left_step_params_cylce(int cycles, int sleep_time);
void step_monitor_exe_right_step_params_cylce(int cycles, int sleep_time);


#define MOTOR_LEFT_STEP_GPIO    0
#define MOTOR_LEFT_DIR_GPIO     1

#define MOTOR_RIGHT_STEP_GPIO   2
#define MOTOR_RIGHT_DIR_GPIO    10

#endif