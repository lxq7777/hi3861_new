#ifndef __MYAPP_LOG_H__
#define __MYAPP_LOG_H__

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

typedef enum
{
    MYAPP_LOG_NONE,   /*!< No log output */
    MYAPP_LOG_ERROR,  /*!< Critical errors, software module can not recover on its own */
    MYAPP_LOG_WARN,   /*!< Error conditions from which recovery measures have been taken */
    MYAPP_LOG_INFO,   /*!< Information messages which describe normal flow of events */
    MYAPP_LOG_DEBUG,  /*!< Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
    MYAPP_LOG_VERBOSE /*!< Bigger chunks of debugging information, or frequent messages which can potentially flood the output. */
} esp_log_level_t;

// info level
#define LOG_LOCAL_LEVEL 3

#define LOG_FORMAT(letter, format)   #letter " (%d) %s: " format "\n"

int myapp_log_timestamp(int times);

void myapp_log_write(esp_log_level_t level, const char* tag, const char* format, ...) __attribute__ ((format (printf, 3, 4)));

static int ERROR_TIMES = 0;
#define MYAPP_LOGE( tag, format, ... )  if (LOG_LOCAL_LEVEL >= MYAPP_LOG_ERROR)   { myapp_log_write(MYAPP_LOG_ERROR,   tag, LOG_FORMAT(ERROR, format), myapp_log_timestamp(ERROR_TIMES++), tag, ##__VA_ARGS__); }
static int WARN_TIMES = 0;
#define MYAPP_LOGW( tag, format, ... )  if (LOG_LOCAL_LEVEL >= MYAPP_LOG_WARN)    { myapp_log_write(MYAPP_LOG_WARN,   tag, LOG_FORMAT(WARN, format), myapp_log_timestamp(WARN_TIMES++), tag, ##__VA_ARGS__); }
static int INFO_TIMES = 0;
#define MYAPP_LOGI( tag, format, ... )  if (LOG_LOCAL_LEVEL >= MYAPP_LOG_INFO)    { myapp_log_write(MYAPP_LOG_INFO,   tag, LOG_FORMAT(INFO, format), myapp_log_timestamp(INFO_TIMES++), tag, ##__VA_ARGS__); }
static int DEBUG_TIMES = 0;
#define MYAPP_LOGD( tag, format, ... )  if (LOG_LOCAL_LEVEL >= MYAPP_LOG_DEBUG)   { myapp_log_write(MYAPP_LOG_DEBUG,   tag, LOG_FORMAT(DEBUG, format), myapp_log_timestamp(DEBUG_TIMES++), tag, ##__VA_ARGS__); }
static int VERBOSE_TIMES = 0;
#define MYAPP_LOGV( tag, format, ... )  if (LOG_LOCAL_LEVEL >= MYAPP_LOG_VERBOSE) { myapp_log_write(MYAPP_LOG_VERBOSE,   tag, LOG_FORMAT(VERBOSE, format), myapp_log_timestamp(VERBOSE_TIMES++), tag, ##__VA_ARGS__); }


#endif