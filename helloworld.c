#include "main_header.h"

static const char *TAG = "MAIN";

#define TASK_STACK_SIZE 1024 * 40
#define TASK_PRIORITY 25

osThreadId_t TASK_ID = NULL;

UartDefConfig uartDefConfig = {0};

static void NetDemoTask(void)
{
    int netId = ConnectToHotspot();

    int timeout = 10; /* timeout 10ms */
    while (timeout--) {
        printf("After %d seconds, I will start lwip test!\r\n", timeout);
        osDelay(100); /* 延时100ms */
    }

    NetDemoTest(PARAM_SERVER_PORT, PARAM_SERVER_ADDR);

    printf("disconnect to AP ...\r\n");
    DisconnectWithHotspot(netId);
    printf("disconnect to AP done!\r\n");
}


static void *UartTask(const char *arg)
{
    (void)arg;

    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
    hi_unref_param(arg);
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioConfig();
    while (1)
    {
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);
        if ((uartDefConfig.g_uartLen > 0))
        {
            // if (GetUartConfig(UART_RECEIVE_FLAG) == HI_FALSE)
            // {
            (void)memcpy_s(uartDefConfig.g_receiveUartBuff, uartDefConfig.g_uartLen,
                           uartBuff, uartDefConfig.g_uartLen);
            printf("Rec:");
            for (int i = 0; i < uartDefConfig.g_uartLen; i++)
            {
                printf("%x", *(uartDefConfig.g_receiveUartBuff + i));
            }
            printf("\n");

            //     (void)SetUartRecvFlag(UART_RECV_TRUE);
            // }
        }
        TaskMsleep(40); /* 20:sleep 20ms */
    }
}

static void *StepTask(const char *arg)
{
    (void)arg;

    MYAPP_LOGD(TAG, "Begin to start STEP MOTOR TEST!");
    step_motor_init();

    step_monitor_set_dir_up();
    step_monitor_exe_both_step_params_cylce(100, 300);
    step_monitor_set_dir_left();
    step_monitor_exe_both_step_params_cylce(100, 300);
    step_monitor_set_dir_down();
    step_monitor_exe_both_step_params_cylce(100, 300);
    step_monitor_set_dir_right();
    step_monitor_exe_both_step_params_cylce(100, 300);

    return NULL;
}

static void HelloMain(void)
{
    UartInit();
    MYAPP_LOGI(TAG, "Program Start");

    // 初始化进程
    osThreadAttr_t attr;
    // 设置进程名称
    attr.name = "TestTask";
    // 设置分离式进程
    attr.attr_bits = 0U;
    // cb控制块指针设置自动管理(空)
    attr.cb_mem = NULL;
    // cb控制块的大小为0
    attr.cb_size = 0U;
    // 栈管理 指针-> NULL 使用线程管理从内存池分配内存
    attr.stack_mem = NULL;
    // 栈大小 单位:字节
    attr.stack_size = TASK_STACK_SIZE;
    // 进程优先级 Normal+1级
    attr.priority = TASK_PRIORITY;

    if (TASK_ID = osThreadNew((osThreadFunc_t)UartTask, NULL, &attr) == NULL)
    {
        MYAPP_LOGE(TAG, "Failed to create TestTask!\n");
    }
    else
    {
        MYAPP_LOGI(TAG, "Success to create TestTask!%d\n", TASK_ID);
    }
}

SYS_RUN(HelloMain);