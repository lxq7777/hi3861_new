#include "uart.h"

void UartInit(void)
{
    IotUartAttribute uartAttr = {
        .baudRate = 921600, /* baudRate: 115200 */
        .dataBits = 8,      /* dataBits: 8bits */
        .stopBits = 1,      /* stop bit */
        .parity = 0,
    };

    int ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS)
    {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
}

void Uart1GpioConfig(void)
{
    // ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    /* IOT_BOARD */
#elif defined(EXPANSION_BOARD)
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
#endif
}

int SetUartRecvFlag(UartRecvDef def,UartDefConfig uartDefConfig)
{
    if (def == UART_RECV_TRUE)
    {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    }
    else
    {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }

    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type,UartDefConfig uartDefConfig)
{
    int receive = 0;

    switch (type)
    {
    case UART_RECEIVE_FLAG:
        receive = uartDefConfig.g_uartReceiveFlag;
        break;
    case UART_RECVIVE_LEN:
        receive = uartDefConfig.g_uartLen;
        break;
    default:
        break;
    }
    return receive;
}

void ResetUartReceiveMsg(UartDefConfig uartDefConfig)
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
                   0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}

unsigned char *GetUartReceiveMsg(UartDefConfig uartDefConfig)
{
    return uartDefConfig.g_receiveUartBuff;
}
