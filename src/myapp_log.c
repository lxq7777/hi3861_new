#include "myapp_log.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

void myapp_log_write(esp_log_level_t level,
                   const char *tag,
                   const char *format, ...)
{
    va_list arg;
    va_start(arg, format);
    vprintf(format, arg);
    va_end(arg);
}

int myapp_log_timestamp(int times){
	return times;
}
